package pallares.didac.ejerciciopantallatiempofragments;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;

public class MainActivity extends AppCompatActivity implements BlankFragment.OnFragmentInteractionListener {

    RelativeLayout rl;

    // DEFINIMOS LAS VARIABLES PARA RECOGER LA INFORMACION.
    private String city_name = "";
    private String temperature = "";
    private String temperatureType = "";
    private int weatherIcon = 1;
    private String humidity = "";
    private String temp_feel_like = "";

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getTemperatureType() {
        return temperatureType;
    }

    public void setTemperatureType(String temperatureType) {
        this.temperatureType = temperatureType;
    }

    public int getWeatherIcon() {
        return weatherIcon;
    }

    public void setWeatherIcon(int weatherIcon) {
        this.weatherIcon = weatherIcon;
    }

    public String getHumidity() {
        return humidity;
    }

    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }

    public String getTemp_feel_like() {
        return temp_feel_like;
    }

    public void setTemp_feel_like(String temp_feel_like) {
        this.temp_feel_like = temp_feel_like;
    }

    // CREAMOS EDITTEXT Y SPINNERS PARA RECOGER LA INFORMACION.
    EditText et_city_name;
    EditText et_temperature;
    EditText et_humedad;
    EditText et_tempSensation;
    Spinner sp_temperatureType;
    Spinner sp_weatherIcon;

    public EditText getEt_city_name() {
        return et_city_name;
    }

    public void setEt_city_name(EditText et_city_name) {
        this.et_city_name = et_city_name;
    }

    public EditText getEt_temperature() {
        return et_temperature;
    }

    public void setEt_temperature(EditText et_temperature) {
        this.et_temperature = et_temperature;
    }

    public EditText getEt_humedad() {
        return et_humedad;
    }

    public void setEt_humedad(EditText et_humedad) {
        this.et_humedad = et_humedad;
    }

    public EditText getEt_tempSensation() {
        return et_tempSensation;
    }

    public void setEt_tempSensation(EditText et_tempSensation) {
        this.et_tempSensation = et_tempSensation;
    }

    public Spinner getSp_temperatureType() {
        return sp_temperatureType;
    }

    public void setSp_temperatureType(Spinner sp_temperatureType) {
        this.sp_temperatureType = sp_temperatureType;
    }

    public Spinner getSp_weatherIcon() {
        return sp_weatherIcon;
    }

    public void setSp_weatherIcon(Spinner sp_weatherIcon) {
        this.sp_weatherIcon = sp_weatherIcon;
    }

    // CREAMOS LOS COMPONENTES PARA LLAMAR AL FRAGMENT.
    FragmentManager fm;
    FragmentTransaction ft;
    BlankFragment blank;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // AL CLICAR EN ACEPTAR SE RECOGE LA INFORMACION, SE CREA EL FRAGMENT Y SE LLAMA.
        Button aceptar = (Button) findViewById(R.id.mainActivity_aceptar);
        aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                rl = (RelativeLayout) findViewById(R.id.mainActivity_Layout);

                // INSTANCIAMOS LOS EDITTEXT
                setEt_city_name((EditText) findViewById(R.id.mainActivity_nombreCiudad));
                setCity_name(et_city_name.getText().toString());

                setEt_temperature((EditText) findViewById(R.id.mainActivity_temperatureValor));
                setTemperature(et_temperature.getText().toString());

                setEt_humedad((EditText) findViewById(R.id.mainActivity_humedad));
                setHumidity(et_humedad.getText().toString());

                setEt_tempSensation((EditText) findViewById(R.id.mainActivity_temperatureSensation));
                setTemp_feel_like(et_tempSensation.getText().toString());

                /**
                 *  NOTA:
                 *  Los Spinners pueden usar la misma api de Android, si queremos modificarlo, podemos usar
                 *  un Adapter de internet, usando el codigo inferior y poniendo en el archivo gradle la libreria a usar.
                 *
                 *  ArrayAdapter<?> adapterTempType = ArrayAdapter.createFromResource(MainActivity.this, R.array.temperatureSimbol, android.R.layout.simple_spinner_dropdown_item);
                 *  adapterTempType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                 *  sp_temperatureType.setAdapter(adapterTempType);
                 *
                 */

                // INSTANCIAMOS LOS SPINNERS
                setSp_temperatureType((Spinner) findViewById(R.id.mainActivity_spinnerTemperatureSymbol));
                setTemperatureType(sp_temperatureType.getSelectedItem().toString());

                setSp_weatherIcon((Spinner) findViewById(R.id.mainActivity_spinnerWeather));
                setWeatherIcon(sp_weatherIcon.getSelectedItemPosition());


                // COMPROBACION DE ERRORES (TEXTO VACIO).
                if (city_name.isEmpty()) {
                    getEt_city_name().setError("Escribe el Nombre de la Ciudad");
                } else if (temperature.isEmpty()) {
                    getEt_temperature().setError("Escribe la Temperatura");
                } else if (humidity.isEmpty()) {
                    getEt_humedad().setError("Escribe la Humedad");
                } else if (temp_feel_like.isEmpty()) {
                    getEt_tempSensation().setError("Escribe la Sensacion de Temperatura");
                } else {

                    // CREAMOS EL FRAGMENT!
                    fm = getSupportFragmentManager();
                    ft = fm.beginTransaction();
                    blank = BlankFragment.newInstance(city_name, temperature, temperatureType, weatherIcon, temp_feel_like, humidity);
                    ft.replace(R.id.mainActivity_Layout, blank);

                    ft.addToBackStack(null);

                    // LANZAMOS EL FRAGMENT!
                    ft.commit();
                    rl.setVisibility(View.GONE);
                }
            }
        });

        Button reset = (Button) findViewById(R.id.mainActivity_cleanButton);
        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("valor", "entramos al reset!");
                setCity_name("");
                setTemperature("");
                setHumidity("");
                setTemp_feel_like("");
            }
        });
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        fm.popBackStack();
        rl.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.e("valor", "estoy en onStop Activity!");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.e("valor", "estoy en onStart Activity!");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.e("valor", "estoy en onPause Activity!");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.e("valor", "estoy en onRestart Activity!");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("valor", "estoy en onResume Activity!");
    }
}
