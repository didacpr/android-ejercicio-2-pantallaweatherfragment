package pallares.didac.ejerciciopantallatiempofragments;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Random;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link BlankFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link BlankFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BlankFragment extends Fragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    private static final String ARG_PARAM4 = "param4";
    private static final String ARG_PARAM5 = "param5";
    private static final String ARG_PARAM6 = "param6";


    // TODO: Rename and change types of parameters
    private String city_name;
    private String temperature;
    private String temperatureSymbol;
    private int weatherIcon;
    private String feel_like;
    private String humidity;

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BlankFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static BlankFragment newInstance(String param1, String param2, String param3, int param4, String param5, String param6) {
        BlankFragment fragment = new BlankFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        args.putString(ARG_PARAM3, param3);
        args.putInt(ARG_PARAM4, param4);
        args.putString(ARG_PARAM5, param5);
        args.putString(ARG_PARAM6, param6);
        fragment.setArguments(args);
        return fragment;
    }

    public BlankFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            city_name = getArguments().getString(ARG_PARAM1);
            temperature = getArguments().getString(ARG_PARAM2);
            temperatureSymbol = getArguments().getString(ARG_PARAM3);
            weatherIcon = getArguments().getInt(ARG_PARAM4);
            feel_like = getArguments().getString(ARG_PARAM5);
            humidity = getArguments().getString(ARG_PARAM6);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_blank, container, false);

        TextView tv_city_name = (TextView) v.findViewById(R.id.fragment_cityName);
        tv_city_name.setText(city_name);

        TextView tv_temperature = (TextView) v.findViewById(R.id.fragment_temperatureNumber);
        tv_temperature.setText(temperature);

        TextView tv_temperature_symbol = (TextView) v.findViewById(R.id.fragment_temperatureSymbol);
        tv_temperature_symbol.setText(temperatureSymbol);

        ImageView tv_day_symbol = (ImageView) v.findViewById(R.id.fragment_iconWeather);

        switch (weatherIcon) {
            case 0:
                tv_day_symbol.setImageResource(R.drawable.little_image_sunny);
                break;
            case 1:
                tv_day_symbol.setImageResource(R.drawable.little_image_overcast);
                break;
            case 2:
                tv_day_symbol.setImageResource(R.drawable.little_image_rain);
                break;

            case 3:
                tv_day_symbol.setImageResource(R.drawable.little_image_snow);
                break;

            default:

                break;
        }

        TextView tv_feel_like = (TextView) v.findViewById(R.id.fragment_feelLikeValor);
        tv_feel_like.setText(feel_like + temperatureSymbol);

        TextView tv_humidity = (TextView) v.findViewById(R.id.fragment_humidityValor);
        tv_humidity.setText(humidity + "%");

        Button btn_day_one = (Button) v.findViewById(R.id.fragment_btn_dayOne);
        Button btn_day_two = (Button) v.findViewById(R.id.fragment_btn_dayTwo);
        Button btn_day_three = (Button) v.findViewById(R.id.fragment_btn_dayThree);
        Button btn_day_four = (Button) v.findViewById(R.id.fragment_btn_dayFour);
        Button btn_day_five = (Button) v.findViewById(R.id.fragment_btn_dayFive);
        Button btn_day_six = (Button) v.findViewById(R.id.fragment_btn_daySix);
        Button btn_day_seven = (Button) v.findViewById(R.id.fragment_btn_daySeven);

        mini_weather_options_random(btn_day_one);
        mini_weather_options_random(btn_day_two);
        mini_weather_options_random(btn_day_three);
        mini_weather_options_random(btn_day_four);
        mini_weather_options_random(btn_day_five);
        mini_weather_options_random(btn_day_six);
        mini_weather_options_random(btn_day_seven);

        mini_weather_day_of_the_week(btn_day_one, 0);
        mini_weather_day_of_the_week(btn_day_two, 1);
        mini_weather_day_of_the_week(btn_day_three, 2);
        mini_weather_day_of_the_week(btn_day_four, 3);
        mini_weather_day_of_the_week(btn_day_five, 4);
        mini_weather_day_of_the_week(btn_day_six, 5);
        mini_weather_day_of_the_week(btn_day_seven, 6);


        // Inflate the layout for this fragment
        return v;
    }

    private void mini_weather_options_random(Button btn) {
        Random r = new Random();
        int valorDado = r.nextInt(4);
        //Log.e("valorDado", String.valueOf(valorDado));
        switch (valorDado) {
            case 0:
                btn.setBackgroundResource(R.drawable.mini_image_sunny);
                break;
            case 1:
                btn.setBackgroundResource(R.drawable.mini_image_overcast);
                break;
            case 2:
                btn.setBackgroundResource(R.drawable.mini_image_rain);
                break;
            case 3:
                btn.setBackgroundResource(R.drawable.mini_image_snow);
                break;
            default:
                break;
        }
    }

    private void mini_weather_day_of_the_week(Button btn, int add) {
        GregorianCalendar cal = new GregorianCalendar();
        int today = cal.get(Calendar.DAY_OF_WEEK);

        today = today + add;
        today = today % 7;

        //Log.e("valor today", String.valueOf(today));

        switch (today) {

            case 0: // Domingo
                btn.setText("Sun");
                break;

            case 1: // Lunes
                btn.setText("Mon");
                break;

            case 2: // Martes
                btn.setText("Tue");
                break;

            case 3: // Miercoles
                btn.setText("Wed");
                break;

            case 4: // Jueves
                btn.setText("Thu");
                break;

            case 5: // Viernes
                btn.setText("Fri");
                break;

            case 6: // Sabado
                btn.setText("Sat");
                break;

            default:
                btn.setText("");
                break;

        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    /*
    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e("valor", "estoy en onDestroy Fragment!");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.e("valor", "estoy en onStop Fragment!");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.e("valor", "estoy en onPause Fragment!");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e("valor", "estoy en onResume Fragment!");
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.e("valor", "estoy en onStart Fragment!");
    }
    */
}
